<!--
  ~ Copyright (C) 2011 Brockmann Consult GmbH (info@brockmann-consult.de)
  ~
  ~ This program is free software; you can redistribute it and/or modify it
  ~ under the terms of the GNU General Public License as published by the Free
  ~ Software Foundation; either version 3 of the License, or (at your option)
  ~ any later version.
  ~ This program is distributed in the hope that it will be useful, but WITHOUT
  ~ ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  ~ FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
  ~ more details.
  ~
  ~ You should have received a copy of the GNU General Public License along
  ~ with this program; if not, see http://www.gnu.org/licenses/
  -->

<module>
    <manifestVersion>1.0.0</manifestVersion>
    <symbolicName>beam-core</symbolicName>
    <version>5.0</version>
    <name>BEAM Core Library</name>


    <description>This is the base module of BEAM containing its core functionalities.</description>

    <vendor>Brockmann Consult GmbH</vendor>
    <contactAddress>Max Planck Str.2, 21502 Geesthacht (Germany)</contactAddress>
    <copyright>(C) 2014 by Brockmann Consult GmbH</copyright>
    <url>http://envisat.esa.int/beam</url>
    <licenseUrl>http://www.gnu.org/licenses/gpl.html</licenseUrl>

    <dependency>
        <module>ceres-core</module>
    </dependency>
    <dependency>
        <module>ceres-glayer</module>
    </dependency>
    <dependency>
        <module>ceres-jai</module>
    </dependency>

    <changelog>
        <![CDATA[
        <b>Changes in 5.0</b><br/>
        [BEAM-1634] Use IF,THEN,ELSE in band maths expressions<br/>
        [BEAM-1455] Flag-Coding remains when subsetting<br/>
        [BEAM-1605] 'InvalidArgumentException: maxima' error message when trying to open a band<br/>
        [BEAM-1474] Data type raw symbols in band maths do not consider the raw data type of the raster<br/>
        [BEAM-276]  Product flipping removes important metadata attributes<br/>
        [BEAM-1452] Log-scaling is not correctly applied in scatter plot<br/>
        [BEAM-1527] Add missing Standard Deviation nonlinear image filter<br/>
        [BEAM-953]  No-Data is disregarded in filtered band.<br/>
        [BEAM-1514] ProductUtils.getScanLineTime fails for products with height 1<br/>
        [BEAM-1513] BEAM shall enable users to use Modified Julian Date (MJD) in band arithmetics<br/>
        [BEAM-1456] All bands shall use same image pyramid model<br/>
        [BEAM-1420] Support (A)ATSR Autumn 2012 reprocessed data products<br/>
        [BEAM-1126] Pixel-geocoding comsumes to much memory<br/>
        [BEAM-1471] The time extraction from file name should support DAY_OF_YEAR<br/>
        [BEAM-1447] Dimap reader\writer handling very very large files<br/>
        ]]>
    </changelog>


    <activator>org.esa.beam.BeamCoreActivator</activator>

    <categories>System,Library</categories>

    <!--
       todo - Describe configuration elements
    -->
    <extensionPoint id="parameterEditors"/>

    <extensionPoint id="rgbProfiles">
        <rgbProfile>
            <name type="java.lang.String"/>
            <red type="java.lang.String"/>
            <green type="java.lang.String"/>
            <blue type="java.lang.String"/>
            <alpha type="java.lang.String"/>
            <!-- optional -->
        </rgbProfile>
    </extensionPoint>


    <extension point="ceres-core:serviceProviders">
        <serviceProvider>org.esa.beam.dataio.dimap.spi.DimapPersistableSpi</serviceProvider>
        <serviceProvider>org.esa.beam.framework.dataio.ProductReaderPlugIn</serviceProvider>
        <serviceProvider>org.esa.beam.framework.dataio.ProductWriterPlugIn</serviceProvider>
        <serviceProvider>org.esa.beam.framework.dataop.maptransf.MapTransformDescriptor</serviceProvider>
        <serviceProvider>org.esa.beam.framework.dataop.dem.ElevationModelDescriptor</serviceProvider>
        <serviceProvider>org.esa.beam.framework.datamodel.PointingFactory</serviceProvider>
        <serviceProvider>org.esa.beam.framework.datamodel.PlacemarkDescriptor</serviceProvider>
        <serviceProvider>org.geotools.referencing.operation.MathTransformProvider</serviceProvider>
    </extension>

</module>
