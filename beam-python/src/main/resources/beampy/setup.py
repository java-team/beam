#!/usr/bin/env python3

from distutils.core import setup

setup(name='beampy',
      version='0.4',
      description='BEAM Python API',
      author='Norman Fomferra, Brockmann Consult GmbH',
      py_modules=['beampy'],
      )
